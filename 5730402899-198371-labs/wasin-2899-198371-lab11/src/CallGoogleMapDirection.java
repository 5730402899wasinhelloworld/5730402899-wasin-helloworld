
import java.io.InputStream;
import java.net.URL;
import javax.json.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mark
 */

public class CallGoogleMapDirection {

    public static void main(String[] args) {
        try {
            URL url = new URL("https://maps.googleapis.com/maps/api/directions/json?origin=Khon%20Kaen&destination=Bangkok&key=AIzaSyBcwmgfhvdlTybXw9-AABMdmsjtx59K8g4");
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject obj = rdr.readObject();
            JsonObject obj1 = (JsonObject) obj.getJsonArray("routes").get(0);
            JsonObject obj2 = (JsonObject) obj1.getJsonArray("legs").get(0);
            JsonArray arr = obj2.getJsonArray("steps");
            int i = 0;
            System.out.println("Durection from Khon Kaen to Bangkok");
            for (JsonObject loca : arr.getValuesAs(JsonObject.class)) {
                i++;
                System.out.println(i + "." + loca.getString("html_instructions"));
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}
