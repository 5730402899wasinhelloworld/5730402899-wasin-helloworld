/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.URL;
import javax.xml.stream.*;

public class CallNationXML {

    public static void main(String[] args) throws Exception {
        try {
            URL u = new URL("https://fb.kku.ac.th/krunapon/xml/nations.xml");
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            int inHeader = 0;
            String name="",location = "";
            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if (parser.getLocalName() == "name") {
                           name = parser.getElementText();
                        }else if(parser.getLocalName() == "location"){
                            location = parser.getElementText();
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        if(parser.getLocalName()=="nation"){
                        System.out.println(name + " is in "+location);
                        }
                        break;
                } // end switch
            } // end for
            parser.close();
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {

        } // end try-catch
    } // end main
}
