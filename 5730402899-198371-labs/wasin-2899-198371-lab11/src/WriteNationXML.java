
import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mark
 */
public class WriteNationXML {

    public static void main(String[] args) throws Exception {
        try {
// output file name
            String fileName = "src\\nations.xml";
// write an output factory
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
// write an xml stream writer
            XMLStreamWriter xtw
                    = xof.createXMLStreamWriter(new FileWriter(fileName));
// xml declaration
            xtw.writeStartDocument();
            xtw.writeStartElement("nations");
            xtw.writeStartElement("nation");
            xtw.writeStartElement("name");
            xtw.writeCharacters("Thailand");
            xtw.writeEndElement(); // end name element
            xtw.writeStartElement("location");
            xtw.writeCharacters("Southeast Asia");
            xtw.writeEndElement(); // end location element
            xtw.writeEndElement(); // end nation element
            xtw.writeStartElement("nation");
            xtw.writeStartElement("name");
            xtw.writeCharacters("USA");
            xtw.writeEndElement(); // end name element
            xtw.writeStartElement("location");
            xtw.writeCharacters("North America");
            xtw.writeEndElement(); // end location element
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeEndDocument();
// write any cached data to the underlying
// output stream
            xtw.flush();
            xtw.close();
        } catch (Exception ex) {

            ex.printStackTrace();
        }
        System.out.println("Done");
    }
}
