/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.json.*;
import javax.json.stream.*;
import java.io.*;
public class writeNation {
    public static void main(String[] args) {
        FileWriter writer = null;
        try {
            writer = new FileWriter("src\\nations.json");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        JsonGenerator generator = Json.createGenerator(writer);
        generator.writeStartObject();
        generator.writeStartArray("nations");
        generator.writeStartObject();
        generator.write("name","Thailand");
        generator.write("location","Southeast Asia");
        generator.writeEnd();
        generator.writeStartObject();
        generator.write("name","USA");
        generator.write("location","North America");
        generator.writeEnd();
        generator.writeEnd();
        generator.writeEnd();
        generator.close();
    }
}
