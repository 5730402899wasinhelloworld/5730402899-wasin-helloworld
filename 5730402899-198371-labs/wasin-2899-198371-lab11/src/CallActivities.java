/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.json.*;
import javax.json.stream.*;
import java.io.*;
import java.io.InputStream;
import java.net.URL;
import javax.json.*;

/**
 *
 * @author Mark
 */
public class CallActivities {

    public static void main(String[] args) {
        FileWriter writer = null;
        try {
            writer = new FileWriter("src\\activities.json");

            JsonGenerator generator = Json.createGenerator(writer);
            generator.writeStartObject();
            generator.writeStartArray("activities");
            URL url = new URL("https://www.kku.ac.th/ikku/api/activities/services/topActivity.php");
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject obj = rdr.readObject();
            JsonArray array = obj.getJsonArray("activities");
            for (JsonObject activi : array.getValuesAs(JsonObject.class)) {

                generator.writeStartObject();
                generator.write("title", activi.getString("title"));
                generator.write("url", activi.getString("url"));
                generator.writeEnd();
            }
            generator.writeEnd();
            generator.writeEnd();
            generator.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}
