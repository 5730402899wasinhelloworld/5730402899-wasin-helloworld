/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wasin.pkg2899.pkg198371.lab11;


import java.io.InputStream;
import java.net.URL;
import javax.json.*;
/**
 *
 * @author Mark
 */
public class CallNation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            URL url = new URL("https://fb.kku.ac.th/krunapon/json/nations.json");
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject obj = rdr.readObject();
            JsonArray array = obj.getJsonArray("nations");
            for(JsonObject nations : array.getValuesAs(JsonObject.class)){
                System.out.println(nations.getString("name")+" is in "+nations.getString("location"));
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
    
}
