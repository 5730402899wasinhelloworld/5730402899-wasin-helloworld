/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.URL;
import javax.xml.stream.*;

/**
 *
 * @author Mark
 */
public class call_news_feed {

    public static void main(String[] args) throws Exception {
        try {
            URL u = new URL("http://www.bangkokbiznews.com/rss/feed/technology.xml");
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader parser = factory.createXMLStreamReader(in);
            int inHeader = 0;
            int i = 0;
            // output file name
            String fileName = "src\\news.xml";
// write an output factory
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
// write an xml stream writer
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter(fileName));
// xml declaration
            xtw.writeStartDocument();
            xtw.writeStartElement("news");
            xtw.writeStartElement("channel");
            String title = "", link = "";
            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        // System.out.println(i+"."+parser.getLocalName());
                        if (parser.getLocalName() == "title") {
                            title = parser.getElementText();
                        } else if (parser.getLocalName() == "link") {
                            link = parser.getElementText();
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        if (parser.getLocalName() == "item") {
                            xtw.writeStartElement("item");
                            xtw.writeStartElement("title");
                            xtw.writeCharacters(title);
                            xtw.writeEndElement();
                            xtw.writeStartElement("link");
                            xtw.writeCharacters(link);
                            xtw.writeEndElement();
                            xtw.writeEndElement();
                        }
                        break;
                } // end switch
            } // end for
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();
            parser.close();
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } catch (IOException ex) {

        } // end try-catch
    } // end main
}
