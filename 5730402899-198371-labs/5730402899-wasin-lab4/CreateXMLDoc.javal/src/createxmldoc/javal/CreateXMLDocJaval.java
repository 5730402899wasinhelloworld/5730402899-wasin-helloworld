package createxmldoc.javal;

import javax.xml.parsers.*;
import java.io.File;
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

public class CreateXMLDocJaval {

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            
            Element root = doc.createElement("profile");
            root.setAttribute("id", "5730402899");
            
            Element nameE = doc.createElement("name");
            Element majorE = doc.createElement("major");
            Element areaE = doc.createElement("area");
            
            Text areaT = doc.createTextNode("Software");
            Text nameT = doc.createTextNode("Wasin Pansophon");
            Text majorT = doc.createTextNode("Computer Engineering");
            Text idT = doc.createTextNode("5730402899");
            
            areaE.appendChild(areaT);
            majorE.appendChild(majorT);
            majorE.appendChild(areaE);
            nameE.appendChild(nameT);
            root.appendChild(nameE);
            root.appendChild(majorE);
            doc.appendChild(root);
            
            OutputStream os = new FileOutputStream("myProfile.xml");
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc), new StreamResult(os));
            System.out.println("File created.");
        } catch (Exception e) {

        }

    }

}
