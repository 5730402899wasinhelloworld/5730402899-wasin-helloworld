/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gettingtherootnode.pkg3.pkg1;
import javax.xml.parsers.*;
import java.io.File;
import org.w3c.dom.*;
/**
 *
 * @author Mark
 */
public class DOMParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
// xmlDoc is the root node of DOM tree
            Document xmlDoc = parser.parse("nation.xml");
// obtain the root element of the document
            Element rootElement = xmlDoc.getDocumentElement();
            System.out.println("The root element name is " + rootElement.getNodeName());
            System.out.println("The root element has attribute id=" +rootElement.getAttribute("id"));
        } catch (Exception e) {

        }

    }
    
}
