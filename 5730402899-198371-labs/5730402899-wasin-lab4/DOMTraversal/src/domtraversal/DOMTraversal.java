/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domtraversal;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class DOMTraversal {

    public static void main(String[] args) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document xmlDoc = parser.parse("nation.xml");
            Element rootElement = xmlDoc.getDocumentElement();
            followNode(rootElement);
        } catch (Exception e) {

        }
    }

    public static void followNode(Node node) throws IOException {
        writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
        if (nextNode != null) {
            followNode(nextNode);
        }
    }

    public static void writeNode(Node node) {
        System.out.print("Node:type is ");
        if (node.getNodeType() == 1) {
            System.out.print("element ");
        } else if (node.getNodeType() == 3) {
            System.out.print("text ");
        }
        System.out.println("name is " + node.getNodeName() + " value is " + node.getNodeValue());
    }
}
