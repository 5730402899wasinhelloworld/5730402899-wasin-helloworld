var fs = require('fs');
var http = require('http');

//create a server object:
http.createServer(function (req, res) {
  fs.readFile('kaokonlakao.txt', 'utf8', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    data = data.replace(/#/g, "<br>#");
    res.write(data); //write a response to the client
    return res.end(); //end the response
  });
}).listen(8080); //the server object listens on port 8080
