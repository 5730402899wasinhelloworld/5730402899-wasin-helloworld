var jsonfile = require('jsonfile')
var file = 'data.json'
var obj = {
  "name": "CM",
  "courses": [
    "198330",
    "198371"
  ],
  "places": {
    "residence": "Khon Kaen",
    "visits": [
      "Songkla",
      "Bangkok"
    ]
  }
}

jsonfile.writeFile(file, obj)
jsonfile.readFile(file, function(err, obj) {
  console.log("=== Output part1: the whole object from reading data.json ===")
  console.log(obj)
  console.log("=== Output part2: the values of the second course and the residence ===")
  console.log("Studying ",obj.courses[1]," living in ",obj.places.residence)

})
