<?php

// create the root node of DOM tree
$fileName = "myProfile.xml";
$doc = new DomDocument('1.0');
// create element 'nation'
$root = $doc->createElement('profile');
// assign the element 'nation' as the root node of the XMLdoc
$doc->appendChild($root);
// set attribute of the root node
$root->setAttribute('id', '5730402899');
$major = $doc->createElement('major');
$majorValue = $doc->createTextNode('Computer Engineering');
$area = $doc->createElement('area');
$areaValue = $doc->createTextNode('Software');
$area->appendChild($areaValue);
$major->appendChild($majorValue);
$major->appendChild($area);
$name = $doc->createElement('name');
$nameValue = $doc->createTextNode('Wasin Pansophon');
// make the text node be the chold of node 'location'
$name->appendChild($nameValue);
// make the element node 'name' be the child of the root node of XMLdoc
$root->appendChild($major);
// make the element node 'location' be the child of root node of XML doc
$root->appendChild($name);
// save DOM tree in an XML file
//$str = $doc->saveXML();
$doc->save($fileName);
echo "Finish writing file $fileName";
?>
