<?php

$xmlDoc = new DOMDocument();
// read content of file book.xml
$xmlDoc->load("nation.xml");
// $x is the root node of book.xml
$x = $xmlDoc->documentElement;
// go to each child node of the root
// node of book.xml
print_node($x);
foreach ($x->childNodes AS $item) {
    print_node($item);
    if ($item->hasChildNodes()) {
        foreach ($item->childNodes as $node) {
            print_node($node);
        }
    }
}

function print_node($node) {
    echo "Node Type is ";
    if ($node->nodeType == 1) {
        echo "element ";
    } elseif ($node->nodeType == 3) {
        echo "text ";
    }
    print "name is " . $node->nodeName . " value is " . $node->nodeValue . "<br/>";
    if ($node->hasChildNodes()) {
        $i = 0;
        foreach ($node->childNodes as $node2) {
            $i++;
        }
        print "node " . $node->nodeName . " has " . $i . " children<br/>";
    }
}

?>