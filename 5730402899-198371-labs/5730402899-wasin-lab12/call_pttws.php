<?php
$wsdl = 'http://www.pttplc.com/webservice/pttinfo.asmx?WSDL';

$client = new SoapClient($wsdl);

$methodName = 'CurrentOilPrice';

$params = array('Language'=>'EN');

$soapAction = 'http://www.pttplc.com/webservice/CurrentOilPrice';


$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));
$sxml = simplexml_load_string($objectResult->CurrentOilPriceResult);
echo "<h1>Current oil price in Thailand</h1>";
echo "<table>";
echo "<tr><th>Product</th><th>Price</th></tr>";
for($i=0;$i<count($sxml);$i++){
  print_r("<tr><td>".(String)$sxml->DataAccess[$i]->PRODUCT."</td><td>".(String)$sxml->DataAccess[$i]->PRICE."</td></tr>");
}
echo "</table>";
 ?>
