<?php
header('Content-Type: application/xml');
$url="http://www.bangkokbiznews.com/rss/feed/technology.xml";
$xml = simplexml_load_file($url);
$writer = new XMLWriter();
$writer->openMemory();
$writer->startDocument('1.0');
$writer->startElement('news');
$writer->writeElement('channel', $xml->channel->title[0]);
foreach ($xml->channel->item as $item) {
  $writer->startElement('item');
  $writer->writeElement('title', $item->title);
  $writer->writeElement('url', $item->link);
  $writer->endElement();
}
$writer->endElement();

$writer->endDocument();
echo $writer->OutputMemory(TRUE);
 ?>
